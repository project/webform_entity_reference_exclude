# Webform Entity Reference Exclude field widget

The default Webform entity reference select field widget allows selecting
specific webforms to be selectable from the list, or all webforms to be
selectable.

This module provides an alternative field widget that allows you to exclude
some specific webform(s) from the list, or exclude specific webform categories.

For a full description of the module, visit the
project page](https://www.drupal.org/project/webform_entity_reference_exclude).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/webform_entity_reference_exclude).


## Requirements

This module requires the following modules:

- [Webform](https://www.drupal.org/project/webform)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. On the form display configuration screen for the entity where you added a
   Webform entity reference field, select the widget "Select list with excluded
   webforms". Click the cog icon to configure this widget, and specify the
   webforms to exclude.


## Maintainers

- Sven Decabooter - [svendecabooter](https://www.drupal.org/u/svendecabooter)
